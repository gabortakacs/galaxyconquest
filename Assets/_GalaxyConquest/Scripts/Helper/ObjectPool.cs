﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
	[SerializeField] protected ObjectPoolEntity _entityPrefab;

	protected Queue<ObjectPoolEntity> _pool = new Queue<ObjectPoolEntity>();

	public int Count { get { return _pool.Count; } }

	public void Init(int amount)
	{
		for (int i = 0; i < amount; ++i)
		{
			_pool.Enqueue(CreateObjectPoolEntity());
		}
	}

	public void Push(ObjectPoolEntity obj)
	{
		obj.Reset();
		obj.gameObject.SetActive(false);
		_pool.Enqueue(obj);
	}

	public virtual ObjectPoolEntity Poll()
	{
		if (Count == 0)
		{
			return null;
		}
		return _pool.Dequeue();
	}

	public ObjectPoolEntity CreateObjectPoolEntity()
	{
		ObjectPoolEntity tmp = Instantiate(_entityPrefab);
		tmp.gameObject.transform.parent = transform;
		tmp.gameObject.SetActive(false);
		tmp.Init();
		return tmp;
	}
}

