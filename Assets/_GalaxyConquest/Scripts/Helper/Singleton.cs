﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T _instance;

	private static object _lock = new object();

	public virtual void Awake()
	{

		T[] instaces = FindObjectsOfType<T>();
		if (instaces.Length > 1)
			Destroy(this);
		_instance = Instance;
	}

	public static T Instance
	{
		get
		{
			lock (_lock)
			{
				if (_instance == null)
				{
					_instance = (T)FindObjectOfType(typeof(T));

					if (FindObjectsOfType(typeof(T)).Length > 1)
					{
						return _instance;
					}

					if (_instance == null)
					{
						GameObject singleton = new GameObject();
						_instance = singleton.AddComponent<T>();
						singleton.name = "(singleton) " + typeof(T).ToString();
					}
				}

				return _instance;
			}
		}
	}

	public static bool IsNull
	{
		get { return _instance == null; }
	}

	public virtual void OnDestroy()
	{
		
	}
}