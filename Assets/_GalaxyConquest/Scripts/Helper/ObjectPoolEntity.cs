﻿using UnityEngine;

public abstract class ObjectPoolEntity : MonoBehaviour 
{
	public abstract void Init();
	public abstract void Reset();
}

