﻿using System;
using UnityEngine;

public class PlanetTrader : Singleton<PlanetTrader> 
{
	private Planet _playerStartingPlanet;
	private Planet _playerDestinationPlanet;
	private TransporterPoolManager _transporterPoolManager;
	public TransporterPoolManager TransporterPoolManager { set { _transporterPoolManager = value; } }

	public void SetPlayerPlanet(Planet planet)
	{
		if (_playerStartingPlanet == null)
		{
			if (planet.OwnerEntityType != EntityType.Player)
				return;
			_playerStartingPlanet = planet;
			_playerStartingPlanet.TargetSignActive = true;
		}
		else
		{
			_playerDestinationPlanet = planet;
			_playerDestinationPlanet.SetDestTarget();
			if (_playerStartingPlanet == _playerDestinationPlanet)
			{
				CancelCommand();
			}
			else
			{
				int changeValue = _playerStartingPlanet.Value / 2;
				PlayerLaunchTransporter(changeValue);
				DeletePlanetTarget();
			}
		}
	}

	public void LaunchTransporter(Planet starter, Planet target, int changeValue)
	{
		if (changeValue == 0)
			return;

		starter.ChangeValue(-changeValue, starter.OwnerEntityType);
		starter.SetTransporterLaunchPointToDirection((target.transform.position - starter.transform.position).normalized);
		Transporter transporter = _transporterPoolManager.Poll();
		transporter.OwnerEntityType = starter.OwnerEntityType;
		transporter.transform.position = starter.TransporterLaunchPoint.position;
		transporter.Target = target;
		transporter.Value = changeValue;
		transporter.SetTransporterHelper(_transporterPoolManager.TransporterHelperParent, target.transform.position, target.transform.lossyScale);
		transporter.gameObject.SetActive(true);
	}

	private void PlayerLaunchTransporter(int changeValue)
	{
		LaunchTransporter(_playerStartingPlanet, _playerDestinationPlanet, changeValue);
	}

	private void CancelCommand()
	{
		DeletePlanetTarget();
	}

	private void DeletePlanetTarget()
	{
		_playerStartingPlanet.TargetSignActive = false;
		_playerStartingPlanet = null;
		_playerDestinationPlanet = null;
	}
}

