﻿using System.Collections;
using UnityEngine;

public class TransporterPoolManager : MonoBehaviour 
{

	[SerializeField] private ObjectPool _transporterPool;
	[SerializeField] private Transform _transporterHelperParent;
	[SerializeField] private int _amount = 100;
	[SerializeField] private int _additionTransporterAmount = 10;

	Coroutine _transporterCreationRoutine = null;

	public Transform TransporterHelperParent { get { return _transporterHelperParent; } }

	private void Awake()
	{
		_transporterPool.Init(_amount);
	}

	public Transporter Poll()
	{
		Transporter tmp = (Transporter)_transporterPool.Poll();
		if (tmp == null)
		{
			if (_transporterCreationRoutine != null)
				StopCoroutine(_transporterCreationRoutine);
			_transporterCreationRoutine = StartCoroutine(CreateTransporters());
			tmp = (Transporter)_transporterPool.CreateObjectPoolEntity();
		}
		return tmp;
	}

	public void Push(Transporter transporter)
	{
		_transporterPool.Push(transporter);
	}

	IEnumerator CreateTransporters()
	{
		for(int i = 0; i < _additionTransporterAmount; ++i)
		{
			_transporterPool.Push(_transporterPool.CreateObjectPoolEntity());
			yield return null;
		}
	}
}

