﻿using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class Transporter : ObjectPoolEntity
{
	[SerializeField] private float _landingDistance = 1.0f;
	[SerializeField] private float _rotationSpeed;
	[SerializeField] private TransportersHelper _transportersHelper;
	[SerializeField] private TextMeshPro _textMeshPro;
	[SerializeField] private NavMeshAgent _navAgent;
	[SerializeField] private GameObject _materialHolder;
	
	private float _speed;
	private Planet _target;
	private int _value;
	private EntityType _ownerEntityType;
	private EntityManager _entityManager;
	private TransporterPoolManager _transporterPoolManager;
	private Material _material;
	private bool _isManualMoving;
	private bool _rotationPositive;

	public Planet Target { set { _target = value; } }

	public EntityType OwnerEntityType
	{
		set
		{
			if (_entityManager == null)
				_entityManager = GameMaster.Instance.ActiveSceneMaster.EntityManager;
			_ownerEntityType = value;
			_materialHolder.GetComponent<Renderer>().material = _entityManager.GetMaterialByType(_ownerEntityType);
		}
	}

	public int Value
	{
		set
		{
			_value = value;
			ChangeModelSize();
			ChangeText();
		}
	}

	private void Awake()
	{
		_navAgent.updateRotation = false;
	}

	private void OnEnable()
	{
		_transportersHelper.gameObject.SetActive(true);
		if (_entityManager == null)
			_entityManager = GameMaster.Instance.ActiveSceneMaster.EntityManager;
	}

	private void Start()
	{
		Init();
		_transporterPoolManager = GameMaster.Instance.ActiveSceneMaster.TransporterPoolManager;
		if (_entityManager == null)
			_entityManager = GameMaster.Instance.ActiveSceneMaster.EntityManager;
	}

	private void Update()
	{
		Rotate();
		Move();
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject == _target.gameObject)
		{
			_target.ChangeValue(_value, _ownerEntityType);
			Reset();
			_transporterPoolManager.Push(this);
		}
	}

	public override void Init()
	{
		_speed = GameMaster.Instance.ActiveSceneMaster.TransporterSpeed;
		_isManualMoving = false;
		_navAgent.speed = _speed;
		_rotationPositive = Random.Range(0, 2) == 0;
	}

	public override void Reset()
	{
		_target = null;
		_value = -1;
		_isManualMoving = false;
		_navAgent.speed = _speed;
	}

	public void SetTransporterHelper(Transform transporterHelperParent, Vector3 position, Vector3 scale)
	{
		_transportersHelper.transform.parent = transporterHelperParent;
		_transportersHelper.transform.position = position;
		_transportersHelper.transform.localScale = scale;
		_transportersHelper.Transporter = this;
	}

	private void Move()
	{
		if (_isManualMoving)
		{
			transform.position = Vector3.MoveTowards(transform.position, _target.transform.position, _speed * Time.deltaTime);
		}
		else
		{
			if (!_navAgent.hasPath || _navAgent.isPathStale || _transportersHelper.LandingStation.position != _navAgent.destination)
			{
				if (_navAgent.isStopped)
					_navAgent.isStopped = false;
				_navAgent.destination = _transportersHelper.LandingStation.position;
			}
			if (_navAgent.hasPath && _navAgent.remainingDistance < _landingDistance)
			{
				_isManualMoving = true;
				_navAgent.isStopped = true;
				transform.position = Vector3.MoveTowards(transform.position, _target.transform.position, _speed * Time.deltaTime);
			}
		}
	}

	private void Rotate()
	{
		Quaternion rotation = transform.rotation;
		Vector3 eulerAngles = transform.rotation.eulerAngles;
		eulerAngles += Vector3.up * ((_rotationPositive ? _rotationSpeed : -_rotationSpeed) * Time.deltaTime);
		rotation.eulerAngles = eulerAngles;
		transform.rotation = rotation;
	}

	private void ChangeText()
	{
		_textMeshPro.text = _value.ToString();
	}

	private void ChangeModelSize()
	{
		float scale = Mathf.Lerp(GameMaster.Instance.ActiveSceneMaster.TransporterMinScale, GameMaster.Instance.ActiveSceneMaster.TransporterMaxScale, (float)_value / 100);
		transform.localScale = Vector3.one * scale;
	}

	private void OnDisable()
	{
		_transportersHelper.gameObject.SetActive(false);
	}
}

