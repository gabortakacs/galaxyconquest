﻿using UnityEngine;

public enum EntityType
{
	Neutral = 0,
	Player = 10,
	AIRed = 20,
	AIYellow = 21,
	AIBlue = 22
}

public class EntityManager : MonoBehaviour
{
	[SerializeField] private Material _materialPlayer;
	[SerializeField] private Material _materialNeutral;
	[SerializeField] private Material _materialAIRed;
	[SerializeField] private Material _materialAIYellow;
	[SerializeField] private Material _materialAIBlue;
	[SerializeField] private Material _default;

	public Material GetMaterialByType(EntityType type)
	{
		switch (type)
		{
			case EntityType.Player:
				return _materialPlayer;
			case EntityType.Neutral:
				return _materialNeutral;
			case EntityType.AIRed:
				return _materialAIRed;
			case EntityType.AIYellow:
				return _materialAIYellow;
			case EntityType.AIBlue:
				return _materialAIBlue;
		}
		return _default;
	}

}

