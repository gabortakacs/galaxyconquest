﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public delegate void PlanetConquested(Planet planet);

public class Planet : MonoBehaviour 
{

	[SerializeField] private int _value;
	[SerializeField] private int _maxValue = 150;
	[SerializeField] private float _incrementPerSec = 1.0f;
	[SerializeField] private bool _isGrowing = true;
	[SerializeField][Range(0.5f, 3.0f)] private float _sizeMultiplier = 1.0f;

	[SerializeField] private float _targetedSignDuration = 0.5f;
	[SerializeField] private EntityType _ownerEntityType;
	[SerializeField] private Transform _transporterLaunchHolder;
	[SerializeField] private Transform _transporterLaunchPoint;
	[SerializeField] private TextMeshPro _textMeshPro;
	[SerializeField] private GameObject _targetSign;
	[SerializeField] private GameObject _targetedSign;
	[SerializeField] private NavMeshObstacle _navMeshObstacle;
	[SerializeField] private Renderer _matHolderRenderer;

	private TransporterPoolManager _transporterPoolManager;
	private Coroutine _targetedSignRoutine = null;

	public static event PlanetConquested OnPlanetConquested;

	public int Value { get { return _value; } }
	public bool TargetSignActive { set { _targetSign.SetActive(value); } }
	public Transform TransporterLaunchPoint { get { return _transporterLaunchPoint; } }
	public EntityType OwnerEntityType { get { return _ownerEntityType; } }
	private SceneMaster SceneMaster
	{
		get
		{
			return GameMaster.Instance.ActiveSceneMaster;
		}
	}

	private void Start()
	{
		if (_incrementPerSec != 0)
			InvokeRepeating("IncrementValueByOne", 1.0f, 1/_incrementPerSec);
		GameMaster.Instance.OnSceneLoaded += OnSceneLoaded;
		Init();
	}

	private void OnMouseDown()
	{
		PlanetTrader.Instance.SetPlayerPlanet(this);
	}

	public void SetTransporterLaunchPointToDirection(Vector3 direction)
	{
		_transporterLaunchHolder.rotation = Quaternion.LookRotation(direction);
	}

	public void ChangeValue(int amount, EntityType attacker)
	{

		if (_ownerEntityType == attacker)
		{
			_value += amount;
			if (_value > _maxValue)
				_value = _maxValue;
		}
		else
		{
			_value -= amount;
			if (_value < 0)
			{
				PlanetConquested(attacker);
			}
		}
		_textMeshPro.text = _value.ToString();
		ChangeModelSize();
	}

	private void PlanetConquested(EntityType attacker)
	{
		_value *= -1;
		_ownerEntityType = attacker;
		ChangeMaterial();
		_isGrowing = true;
		if (OnPlanetConquested != null)
			OnPlanetConquested.Invoke(this);
	}

	public void SetDestTarget()
	{
		if (_targetedSignRoutine != null)
			StopCoroutine(_targetedSignRoutine);
		_targetedSignRoutine = StartCoroutine(DestTargetRoutine());
	}

	IEnumerator DestTargetRoutine()
	{
		_targetedSign.SetActive(true);
		yield return new WaitForSeconds(_targetedSignDuration);
		_targetedSign.SetActive(false);
	}

	private void OnSceneLoaded(int index)
	{
		if (index != 0)
		{
			ChangeValue(0, _ownerEntityType);
		}
	}

	private void ChangeModelSize()
	{
		float scale = GetPlanetScale();
		float radius = GetNavMeshObstacleRadius();
		transform.localScale = Vector3.one * scale;
		_navMeshObstacle.radius = radius;
	}

	private void IncrementValueByOne()
	{
		if (!_isGrowing)
			return;
		ChangeValue(1, _ownerEntityType);
	}

	private void Init()
	{
		_textMeshPro.text = _value.ToString();
		ChangeModelSize();
		ChangeMaterial();
	}

	private void ChangeMaterial()
	{
		if (SceneMaster.EntityManager != null)
			_matHolderRenderer.material = SceneMaster.EntityManager.GetMaterialByType(_ownerEntityType);
	}

	private void OnDestroy()
	{
		if (!GameMaster.IsNull)
			GameMaster.Instance.OnSceneLoaded -= OnSceneLoaded;
	} 

	private float GetPlanetScale()
	{
		return Mathf.Lerp(SceneMaster.PlanetMinScale * _sizeMultiplier, SceneMaster.PlanetMaxScale * _sizeMultiplier, (float)_value / 100);
	}

	private float GetNavMeshObstacleRadius()
	{
		return 0.65f;
	}

#if UNITY_EDITOR_WIN
	public void OnValidate()
	{
		if (Application.isPlaying)
		{
			ChangeMaterial();
			if (OnPlanetConquested != null)
				OnPlanetConquested.Invoke(this);
			return;
		}
		else if (SceneManager.GetActiveScene().buildIndex == 0 ||  SceneMaster == null)
			return;
		_textMeshPro.text = _value.ToString();

		float scale = GetPlanetScale();
		float radius = GetNavMeshObstacleRadius();
		transform.localScale = Vector3.one * scale;
		_navMeshObstacle.radius = radius;
		ChangeMaterial();
	}
#endif

}

