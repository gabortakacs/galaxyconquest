﻿using UnityEngine;

public class TransportersHelper : MonoBehaviour
{
	[SerializeField] private Transform _landingStation;
	[SerializeField] private float _landingStationPositionRecalculation = 5.0f;
	[SerializeField] private float _changeLandingStationPositionMinAngleDiff = 15.0f;

	private Transporter _transporter;
	private Vector3 _prevDir;

	public Transform LandingStation { get { return _landingStation; } }
	public Transporter Transporter { set { _transporter = value; } }

	private void OnEnable()
	{
		InvokeRepeating("RotateToTarget", 0.0f, _landingStationPositionRecalculation);
		if (_transporter)
			_prevDir = -_transporter.transform.position + transform.position;
	}

	private void OnDisable()
	{
		CancelInvoke("RotateToTarget");
	}

	private void RotateToTarget()
	{
		Vector3 dir = _transporter.transform.position - transform.position;
		float angle = Mathf.Abs(Vector3.Angle(dir, _prevDir));
		if(angle > _changeLandingStationPositionMinAngleDiff)
		{
			transform.rotation = Quaternion.LookRotation(dir);
			_prevDir = dir;
		}
	}
}

