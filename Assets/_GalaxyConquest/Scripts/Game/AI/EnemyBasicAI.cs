﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyBasicAI : MonoBehaviour 
{
	[SerializeField] private EntityType _entityType;
	[SerializeField] private int _minimumPointOnPlanetToAttack = 20;
	[SerializeField] private int _minimumPointOnHighPriortyPlanetToAttack = 20;
	[SerializeField] private int _highPriorityMaxPrio = 9;
	[SerializeField] private float _retargetPeriodTimeSec = 2.0f;
	[SerializeField] private float _attackPeriodTimeSec = 1.0f;
	[SerializeField] private float _firstAttackSec = 2.0f;

	[Header("Planets & Fronts")]
	[SerializeField] private Front[] _fronts;

	private Dictionary<Front, Planet> _frontTargetMap = new Dictionary<Front, Planet>();
	private PlanetTrader _planetTrader;

	private void Awake()
	{
		for(int i = 0; i < _fronts.Length; ++i)
		{
			_fronts[i].Init();
		}
	}

	private void Start()
	{
		Planet.OnPlanetConquested += OnPlanetConquested;
		_planetTrader = PlanetTrader.Instance;
		InvokeRepeating("RecalculateFrontsTarget", 0.5f, _retargetPeriodTimeSec);
		InvokeRepeating("Attack", _firstAttackSec, _attackPeriodTimeSec);
	}

	private void Attack()
	{
		for(int i = 0; i < _fronts.Length; ++i)
		{
			AttackWithFront(_fronts[i]);
		}
		SetAllFrontPrioPlanetAttackedToFalse();
	}

	private void SetAllFrontPrioPlanetAttackedToFalse()
	{
		for (int i = 0; i < _fronts.Length; ++i)
		{
			SetAllPrioPlanetAttackedToFalse(_fronts[i]);
		}
	}

	private void SetAllPrioPlanetAttackedToFalse(Front front)
	{
		for (int i = 0; i < front.PriorityPlanetPrioList.Count; ++i)
		{
			PriorityPlanet tmp = front.PriorityPlanetPrioList[i];
			tmp.isAttacked = false;
		}
	}


	private bool AttackWithFront(Front front)
	{
		Planet target = _frontTargetMap[front];
		if (target == null)
			return false;
		bool attacked = false;
		foreach(PriorityPlanet prioPlanet in front.PriorityPlanetPrioList)
		{
			if(prioPlanet.Planet.OwnerEntityType == _entityType)
			{
				Attack(prioPlanet, target);
				attacked = true;
			}
		}
		return attacked;
	}

	private void Attack(PriorityPlanet prioPlanet, Planet target)
	{
		if (prioPlanet.isAttacked)
			return;
		bool isPrioPlanet = prioPlanet.Prio <= _highPriorityMaxPrio;
		if (isPrioPlanet && prioPlanet.Planet.Value > _minimumPointOnHighPriortyPlanetToAttack || !isPrioPlanet && prioPlanet.Planet.Value > _minimumPointOnPlanetToAttack)
		{
			_planetTrader.LaunchTransporter(prioPlanet.Planet, target, prioPlanet.Planet.Value / 2);
		}
		prioPlanet.isAttacked = true;
	}

	private void RecalculateFrontsTarget()
	{
		if(!CalculateFrontsTarget())
		{
			for(int i = 0; i < _fronts.Length; ++i)
			{
				_frontTargetMap[_fronts[i]] = GetPlayerFirstPlanet();
			}
		}
	}

	private Planet GetPlayerFirstPlanet()
	{
		HashSet<Planet> playerPlanets = GameMaster.Instance.ActiveSceneMaster.PlayerPlanets;
		if (playerPlanets != null)
			foreach (Planet p in playerPlanets)
				return p;
		return null;
	}

	private bool CalculateFrontsTarget()
	{
		for (int i = 0; i < _fronts.Length; ++i)
		{
			Front currentFront = _fronts[i];
			Planet target;
			GetFirstTargetAndCountNotConqestedTargets(currentFront, out target);
			_frontTargetMap[currentFront] = target;
		}
		for(int i = 0; i < _fronts.Length; ++i)
		{
			Planet target = _frontTargetMap[_fronts[i]];
			if (target != null)
				break;
			for (int j = 0; j < _fronts.Length; ++j)
			{
				target = _frontTargetMap[_fronts[j]];
				if (target != null)
					break;
			}
			if (target == null)
				return false;
			_frontTargetMap[_fronts[i]] = target;
		}
		return true;
	}

	private void GetFirstTargetAndCountNotConqestedTargets(Front front, out Planet target)
	{
		for( int i = 0; i < front.PriorityPlanetPrioList.Count; ++i)
		{
			PriorityPlanet prioPlanet = front.PriorityPlanetPrioList[i];
			if (prioPlanet.Planet.OwnerEntityType != _entityType)
			{
				target = prioPlanet.Planet;
				return;
			}
		}
		target = null;
	}

	private void OnPlanetConquested(Planet planet)
	{
		if (planet.OwnerEntityType == _entityType)
		{
			foreach(Front front in _fronts)
			{
				foreach(PriorityPlanet prioPlanet in front.PriorityPlanetPrioList)
				{
					if (planet == prioPlanet.Planet)
						return;
				}
			}
			AddPlanetToPrioPlanets(_fronts[Random.Range(0, _fronts.Length)], planet, int.MaxValue - 1);
		}
	}

	private void AddPlanetToPrioPlanets(Front front, Planet planet, int prio)
	{
		PriorityPlanet tmp = new PriorityPlanet();
		tmp.Planet = planet;
		tmp.Prio = prio;
		front.PriorityPlanetPrioList.Add(tmp);
		PriorityPlanet.Sort(front.PriorityPlanetPrioList);
	}


	private void OnDestroy()
	{
		Planet.OnPlanetConquested -= OnPlanetConquested;
	}
}

[Serializable]
public struct Front
{
	public PriorityPlanet[] PrioPlanentArray;

	[HideInInspector] public List<PriorityPlanet> PriorityPlanetPrioList;

	public void Init()
	{
		PriorityPlanetPrioList = new List<PriorityPlanet>();
		for(int i = 0; i < PrioPlanentArray.Length; ++i)
		{
			PriorityPlanetPrioList.Add(PrioPlanentArray[i]);
		}
		PriorityPlanet.Sort(PriorityPlanetPrioList);
	}
}

[Serializable] 
public struct PriorityPlanet
{
	public Planet Planet;
	public int Prio;
	public bool isAttacked;

	public static PriorityPlanet Init()
	{
		PriorityPlanet priorityPlanet = new PriorityPlanet();
		priorityPlanet.Planet = null;
		priorityPlanet.Prio = int.MaxValue;
		return priorityPlanet;
	}

	public int CompareTo(PriorityPlanet other)
	{
		return Prio.CompareTo(other.Prio);
	}

	public static void Sort(List<PriorityPlanet> list)
	{
		list.Sort(delegate (PriorityPlanet x, PriorityPlanet y)
		{
			return x.CompareTo(y);
		});
	}
}