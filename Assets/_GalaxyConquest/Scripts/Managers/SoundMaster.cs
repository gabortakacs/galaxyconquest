﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMaster : Singleton<SoundMaster> {

	[SerializeField] private AudioSource _backgroundMusic;

	private bool _soundEnabled;

	private List<AudioSource> _loopSounds = new List<AudioSource>();

	public bool SoundEnabled
	{
		get { return _soundEnabled; }
		set
		{
			_soundEnabled = value;
			PlayerPrefs.SetInt("SoundEnabled", value ? 1 : 0);
			if (value)
				StartAllLoopedSounds();
			else
				StopAllLoopedSounds();
		}
	}

	public override void Awake () {
		base.Awake();
		_soundEnabled = PlayerPrefs.GetInt("SoundEnabled", 1) == 0 ? false : true;
		if(_backgroundMusic)
			PlayLoopSound(_backgroundMusic);
	}

	public void PlayLoopSound(AudioSource audio)
	{
		if (_soundEnabled)
		{
			audio.Play();
		}
		_loopSounds.Add(audio);
	}

	public void PlayOneShotSound(AudioSource audio)
	{
		if (_soundEnabled)
		{
			audio.PlayOneShot(audio.clip);
		}
	}
	private void StopAllLoopedSounds()
	{
		for(int i = 0; i < _loopSounds.Count; ++i)
		{
			_loopSounds[i].Stop();
		}
	}

	private void StartAllLoopedSounds()
	{
		for (int i = 0; i < _loopSounds.Count; ++i)
		{
			_loopSounds[i].Play();
		}
	}


}
