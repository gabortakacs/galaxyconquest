﻿using System.Collections.Generic;
using UnityEngine;

public class SceneMaster : MonoBehaviour
{
	[SerializeField] private bool _isMainMenuScene = false;
	[SerializeField] private EntityManager _entityManager;
	[Header("Planet Settings")]
	[SerializeField][Range(1.0f, 2.0f)] private float _planetMinScale = 1.0f;
	[SerializeField][Range(1.0f, 3.0f)] private float _planetMaxScale = 2.0f;

	[Header("Transporter Settings")]
	[SerializeField] private TransporterPoolManager _transporterToolManager;
	[SerializeField][Range(0.5f, 5.0f)] private float _transporterMinScale = 1.0f;
	[SerializeField][Range(0.5f, 5.0f)] private float _transporterMaxScale = 5.0f;
	[SerializeField] private float _transporterSpeed = 5.0f;

	private HashSet<Planet> _playerPlanets = new HashSet<Planet>();
	private HashSet<Planet> _nonPlayerPlanets = new HashSet<Planet>();

	public HashSet<Planet> PlayerPlanets { get { return _playerPlanets; } }
	public bool IsMainMenuScene { get { return _isMainMenuScene; } }
	public EntityManager EntityManager { get { return _entityManager; } }

	public float PlanetMinScale { get { return _planetMinScale; } }
	public float PlanetMaxScale { get { return _planetMaxScale; } }

	public float TransporterSpeed { get { return _transporterSpeed; } }
	public float TransporterMinScale { get { return _transporterMinScale; } }
	public float TransporterMaxScale { get { return _transporterMaxScale; } }
	public TransporterPoolManager TransporterPoolManager { get { return _transporterToolManager; } }

	private void Awake()
	{
		GameMaster.Instance.OnSceneLoaded += OnSceneLoaded;
	}

	public void Start()
	{
		Planet[] planets = FindObjectsOfType<Planet>();
		for(int i = 0; i < planets.Length; ++i)
		{
			if(planets[i].OwnerEntityType == EntityType.Player)
			{
				_playerPlanets.Add(planets[i]);
			}
			else
			{
				_nonPlayerPlanets.Add(planets[i]);
			}
		}
	}

	public void RegiesterOnPlanetConquested()
	{
		Planet.OnPlanetConquested += OnPlanetConquested;
	}

	public void UnRegiesterOnPlanetConquested()
	{
		Planet.OnPlanetConquested -= OnPlanetConquested;
	}

	public void OnPlanetConquested(Planet planet)
	{
		if(planet.OwnerEntityType == EntityType.Player)
		{
			_playerPlanets.Add(planet);
			_nonPlayerPlanets.Remove(planet);
			if (_nonPlayerPlanets.Count == 0)
				GameMaster.Instance.Win();
		}
		else
		{
			_playerPlanets.Remove(planet);
			_nonPlayerPlanets.Add(planet);
			if (_playerPlanets.Count == 0)
				GameMaster.Instance.GameOver();
		}
	}

	private void OnSceneLoaded(int index)
	{
		GameMaster.Instance.ActiveSceneMaster = this;
		if(_transporterToolManager != null)
			PlanetTrader.Instance.TransporterPoolManager = _transporterToolManager;
	}

#if UNITY_EDITOR
	private void OnValidate()
	{
		if (Application.isPlaying)
			return;
		Planet[] planets = FindObjectsOfType<Planet>();
		for (int i = 0; i < planets.Length; ++i)
		{
			planets[i].OnValidate();
		}
	}
#endif
	private void OnDestroy()
	{
		Planet.OnPlanetConquested -= OnPlanetConquested;
		if(!GameMaster.IsNull)
			GameMaster.Instance.OnSceneLoaded -= OnSceneLoaded;
	}
}

