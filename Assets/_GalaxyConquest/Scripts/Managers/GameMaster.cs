﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public delegate void SceneLoaded(int scene);

public class GameMaster : Singleton<GameMaster>
{
	[SerializeField] private int _mainMenuBuildIndex = 0;
	[SerializeField] private SceneMaster _activeSceneMaster;

	private float _normalTimeScale = 1.0f;
	private float _pausedTimeScale = 0.000001f;
	private bool _isPaused = false;
	private bool _isGameOver = false;
	private int _selectedLevel = 0;

	public event SceneLoaded OnSceneLoaded;

	public bool IsPaused { get { return _isPaused; } }
	public bool IsGameOver { get { return _isGameOver; } }
	public int SelectedLevel { set { _selectedLevel = value; } }
	public SceneMaster ActiveSceneMaster {
		get
		{
			if (_activeSceneMaster == null)
			{
				FindAndSetActiveSceneMaster(SceneManager.GetActiveScene());
			}
			return _activeSceneMaster;
		}
		set
		{
			if (value.gameObject.scene != SceneManager.GetActiveScene())
				return;
			if (_activeSceneMaster != null)
			{
				_activeSceneMaster.UnRegiesterOnPlanetConquested();
				_activeSceneMaster.gameObject.SetActive(false);
			}
			_activeSceneMaster = value;
			_activeSceneMaster.gameObject.SetActive(true);
			_activeSceneMaster.RegiesterOnPlanetConquested();
		}
	}

	public override void Awake()
	{
		base.Awake();
		Time.timeScale = _normalTimeScale;
		//PlayerPrefs.SetInt("HighestLevel", 1);
		_selectedLevel = PlayerPrefs.GetInt("HighestLevel", 1);
	}

	private void Start()
	{
		if(MenuMaster.IsNull)
		{
			LoadSceneAsynch(_mainMenuBuildIndex, false);
		}
	}

	public void Win()
	{
		++_selectedLevel;
		if (_selectedLevel >= SceneManager.sceneCountInBuildSettings)
			_selectedLevel = SceneManager.sceneCountInBuildSettings-1;
		PlayerPrefs.SetInt("HighestLevel", _selectedLevel);
		MenuMaster.Instance.Win();
		UnloadSceneAsynch(SceneManager.GetActiveScene().buildIndex);
	}

	public void GameOver()
	{
		_isGameOver = true;
		MenuMaster.Instance.GameOver();
	}

	public void StartGame()
	{
		_isPaused = false;
		_isGameOver = false;
		Time.timeScale = _normalTimeScale;
		LoadSceneAsynch(_selectedLevel, true);
	}

	public void BackToMainMenu()
	{
		int actScene = SceneManager.GetActiveScene().buildIndex;
		if(actScene != 0)
			UnloadSceneAsynch(SceneManager.GetActiveScene().buildIndex);
	}

	public void Pause()
	{
		_isPaused = true;
		Time.timeScale = _pausedTimeScale;
	}

	public void Resume()
	{
		Time.timeScale = _normalTimeScale;
		_isPaused = false;
	}

	public void RestartScene()
	{
		UnloadSceneAsynch(_selectedLevel);
		StartGame();
	}

	public void QuitGame()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	private void LoadSceneAsynch(int index, bool setActive)
	{
		StartCoroutine(LoadAsyncSceneRoutine(index, setActive));
	}

	private void UnloadSceneAsynch(int index)
	{
		if (index != 0)
			SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(_mainMenuBuildIndex));
		SceneManager.UnloadSceneAsync(index);
	}

	IEnumerator LoadAsyncSceneRoutine(int index, bool setActive)
	{
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(index, LoadSceneMode.Additive);
		while (!asyncLoad.isDone)
		{
			yield return null;
		}
		if (setActive)
		{
			Scene loadedScene = SceneManager.GetSceneByBuildIndex(index);
			SceneManager.SetActiveScene(loadedScene);
		}
		OnSceneLoaded.Invoke(index);
	}

	private void FindAndSetActiveSceneMaster(Scene scene)
	{
		SceneMaster[] sceneMasters = FindObjectsOfType<SceneMaster>();
		for (int i = 0; i < sceneMasters.Length; ++i)
		{
			if (sceneMasters[i].gameObject.scene == scene)
			{
				_activeSceneMaster = sceneMasters[i];
				_activeSceneMaster.gameObject.SetActive(true);
				_activeSceneMaster.RegiesterOnPlanetConquested();
				break;
			}
		}
	}
}
