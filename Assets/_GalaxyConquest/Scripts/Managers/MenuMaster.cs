﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuMaster : Singleton<MenuMaster>
{
	[SerializeField] private MenuPage _startMenu;
	[SerializeField] private MenuPage _pauseMenu;
	[SerializeField] private MenuPage _mapSelectMenu;
	[SerializeField] private MenuPage _gameOverMenu;
	[SerializeField] private MenuPage _winMenu;
	[SerializeField] private MenuPage _optionsMenu;
	[SerializeField] private Toggle _soundToogle;
	[SerializeField] private GameObject _playerHUD;

	public GameObject GameOverMenu { get { return _gameOverMenu.gameObject; } }
	public bool IsOnMainMenu { get { return _startMenu == null ? false : _startMenu.isActiveAndEnabled; } }

	public override void Awake()
	{
		base.Awake();
		GameMaster.Instance.OnSceneLoaded += OnSceneLoaded;
	}

	public void Start()
	{
		if (GameMaster.Instance.ActiveSceneMaster.IsMainMenuScene)
		{
			SetMenuPageActive(_startMenu);
		}

		if (_soundToogle)
		{
			_soundToogle.isOn = SoundMaster.Instance.SoundEnabled;
		}
	}

	public void Update()
	{
		if (Input.GetButtonDown("Cancel") && !GameMaster.Instance.IsGameOver && !IsOnMainMenu)
		{
			if (!GameMaster.Instance.IsPaused)
				Pause();
			else
				Resume();
		}
	}

	public void StartGame()
	{
		GameMaster.Instance.StartGame();
	}

	public void Resume()
	{
		DeativeAllPages();
		GameMaster.Instance.Resume();
	}

	public void SelectMap()
	{
		SetMenuPageActive(_mapSelectMenu);
	}

	public void QuitGame()
	{
		GameMaster.Instance.QuitGame();
	}

	public void Pause()
	{
		SetMenuPageActive(_pauseMenu);
		GameMaster.Instance.Pause();
	}

	public void Restart()
	{
		GameMaster.Instance.RestartScene();
	}

	public void Options()
	{
		SetMenuPageActive(_optionsMenu);
	}

	public void Back()
	{
		MenuPage nextMenu = null;
		if (GameMaster.Instance.IsPaused)
		{
			nextMenu = _pauseMenu;
		}
		else if (GameMaster.Instance.IsGameOver)
		{
			nextMenu = _gameOverMenu;
		}
		else
		{
			nextMenu = _startMenu;
		}
		SetMenuPageActive(nextMenu);
	}

	public void GameOver()
	{
		DeativeAllPages();
		SetMenuPageActive(_gameOverMenu);
	}

	public void Win()
	{
		DeativeAllPages();
		SetMenuPageActive(_winMenu);
	}

	public void MainMenu()
	{
		SetMenuPageActive(_startMenu);
		GameMaster.Instance.BackToMainMenu();
	}

	public void SoundToogleSwitched(bool toState)
	{
		SoundMaster.Instance.SoundEnabled = toState;
	}

	private void SetMenuPageActive(MenuPage page)
	{
		DeativeAllPages();
		page.gameObject.SetActive(true);
	}

	private void DeativeAllPages()
	{
		if (_gameOverMenu)
			_gameOverMenu.gameObject.SetActive(false);

		if (_optionsMenu)
			_optionsMenu.gameObject.SetActive(false);

		if (_pauseMenu)
			_pauseMenu.gameObject.SetActive(false);

		if (_playerHUD)
			_pauseMenu.gameObject.SetActive(false);

		if (_startMenu)
			_startMenu.gameObject.SetActive(false);

		if (_mapSelectMenu)
			_mapSelectMenu.gameObject.SetActive(false);

		if (_winMenu)
			_winMenu.gameObject.SetActive(false);
	}

	private void OnSceneLoaded(int index)
	{
		if (index != 0)
		{
			DeativeAllPages();
		}
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
		if(!GameMaster.IsNull)
			GameMaster.Instance.OnSceneLoaded -= OnSceneLoaded;
	}
}
