﻿using UnityEngine;
using UnityEngine.UI;

public class MapSelectorButton : MonoBehaviour
{
	[SerializeField] private Button _button;
	[SerializeField] private Text _text;
	private int _mapBuildIndex;
	private bool _isActive;

	public bool IsActive
	{
		set
		{
			_button.interactable = value;
		}
	}

	public void Init(int mapBuildIndex, bool isActive)
	{
		_mapBuildIndex = mapBuildIndex;
		IsActive = isActive;
		_text.text = mapBuildIndex.ToString();
	}

	public void SelectMap()
	{
		GameMaster.Instance.SelectedLevel = _mapBuildIndex;
		GameMaster.Instance.StartGame();
	}
}
