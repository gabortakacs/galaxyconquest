﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MapSelectorMenuPage : MenuPage
{
	[SerializeField] private Transform _mapParent;
	[SerializeField] private MapSelectorButton _buttonPrefab;

	private void Awake()
	{
		int sceneCount = SceneManager.sceneCountInBuildSettings;
		Debug.Log(sceneCount);
		for (int i = 1; i < sceneCount; ++i)
		{
			MapSelectorButton tmp = Instantiate(_buttonPrefab, _mapParent);
			tmp.Init(i, true);
		}
		for (int i = sceneCount; i < 30; ++i)
		{
			MapSelectorButton tmp = Instantiate(_buttonPrefab, _mapParent);
			tmp.Init(i, false);
		}
	}

}
